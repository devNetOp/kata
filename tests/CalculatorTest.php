<?php
require('src/Calculator.php');
class CalculatorTest extends PHPUnit_Framework_TestCase{

    public function testExitingCalculate(){

        $calculator = new Calculator();

        $this->assertTrue(is_a($calculator, 'Calculator'));
    }

    public function testPlus12And34Return46(){

        $calculator = new Calculator();
        $this->assertEquals(46, $calculator->plus('+ 12 34'));
    }
}